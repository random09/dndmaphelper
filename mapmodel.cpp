#include "mapmodel.h"
#include "buildingmodel.h"
#include "assetshelper.h"

MapModel::MapModel(int tilesX, int tilesY, QObject *parent) : MapComponentBase(tilesX, tilesY, parent)
{

}

void MapModel::addBuilding(int xPos, int yPos, BuildingModel building)
{
//    QPoint pos(xPos * AssetsHelper::ASSET_IMAGE_WIDTH, yPos * AssetsHelper::ASSET_IMAGE_HEIGHT);

//    painter.drawImage(pos, building.getComponetImage());
    building.render();
    buildingMap[building] =QPoint(xPos, yPos);
}

//waring resets image
void MapModel::setTerrain(Asset *ter)
{
    terrain = ter;

}

//only works if called before setTerrain
void MapModel::setTerrainEffect(TerrainEffect effect, double scale)
{
    if(scale > 100.0){
        scale = 100.0;
    }

    tEffect = effect;
    tEffectScale = (scale/100.0) * 255;
}

void MapModel::setBuildRoads(bool set)
{
    buildRoads = set;
}

void MapModel::render()
{

    for(int i = 0; i < getTileCountWidth(); i++){
        for(int j = 0; j < getTileCountHeight(); j++){
            drawTile(terrain, i, j);
        }
    }

    if(tEffectScale == 0){
        return;
    }

    switch (tEffect) {
    case NoTerrainEffect:
        break;
    case SnowyTerrain:
        for(int i = 0; i < imgBase.width(); i++){
            for(int j = 0; j < imgBase.height(); j++){
                imgBase.setPixelColor(i,j,QColor::fromRgb(255,255,255,tEffectScale));
            }
        }
        break;
    case BurningTerrain:
        for(int i = 0; i < imgBase.width(); i++){
            for(int j = 0; j < imgBase.height(); j++){
                imgBase.setPixelColor(i,j,QColor::fromRgb(230,46,0,tEffectScale));
            }
        }
        break;

    }


    for(auto iter = buildingMap.begin(); iter != buildingMap.end(); iter++){
        QPoint imgPos(iter.value().x() * AssetsHelper::ASSET_IMAGE_WIDTH, iter.value().y() * AssetsHelper::ASSET_IMAGE_HEIGHT);

        painter.drawImage(imgPos, iter.key().getComponetImage());
    }

    //todo build roads between buildings

    finilaze();
}
