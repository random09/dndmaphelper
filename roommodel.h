#ifndef ROOMMODEL_H
#define ROOMMODEL_H

#include "mapcomponentbase.h"

class RoomModel : public MapComponentBase
{
public:
    explicit RoomModel(int tilesX, int tilesY, QObject *parent = nullptr);
};

#endif // ROOMMODEL_H
