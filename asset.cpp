#include "asset.h"
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include "assetshelper.h"

Asset::Asset(QObject *parent) : QObject(parent)
{

}

Asset::Asset(AssetType type, AssetState state, AssetStyle style, AssetRoomType roomType, QObject *parent): QObject(parent), assetType(type), assetState(state), assetStyle(style), roomType(roomType)
{

}

const QString &Asset::getName() const
{
    return name;
}

void Asset::setName(const QString &newName)
{
    name = newName;
}

const QString &Asset::getImgPath() const
{
    return imgPath;
}

void Asset::setImgPath(const QString &newImgPath)
{
    imgPath = newImgPath;
}

Asset::AssetType Asset::getAssetType() const
{
    return assetType;
}

Asset::AssetState Asset::getAssetState() const
{
    return assetState;
}

Asset::AssetStyle Asset::getAssetStyle() const
{
    return assetStyle;
}

QImage *Asset::getAssetImage()
{
    if(assetImage != nullptr){
        return assetImage;
    }else if(AssetsHelper::getAssetHelper()->getPercentMemUsed() < 80.0){
        assetImage = new QImage(imgPath);
    }

    return new QImage(imgPath);
}

void Asset::destroyImage()
{
    if(assetImage != nullptr){
        delete assetImage;
        assetImage = nullptr;
    }
}

QByteArray Asset::saveToJson(Asset* a)
{
    QJsonObject obj;

    obj["name"] = a->name;
    obj["image_path"] = a->imgPath;
    obj["type"] = static_cast<int>(a->assetType);
    obj["state"] = static_cast<int>(a->assetState);
    obj["style"] = static_cast<int>(a->assetStyle);
    obj["room"] = static_cast<int>(a->roomType);

    return QJsonDocument(obj).toJson(QJsonDocument::Indented);
}

Asset* Asset::loadFromJson(QByteArray b)
{
    QJsonObject obj = QJsonDocument::fromJson(b).object();

    Asset* a = new Asset();

    if(obj["name"].isString()){
        a->name = obj["name"].toString();
    }

    if(obj["image_path"].isString()){
        a->imgPath = obj["image_path"].toString();
    }

    if(obj["type"].isDouble()){
        a->assetType = static_cast<Asset::AssetType>(obj["type"].toDouble());
    }

    if(obj["state"].isDouble()){
        a->assetState = static_cast<Asset::AssetState>(obj["state"].toDouble());
    }

    if(obj["style"].isDouble()){
        a->assetStyle = static_cast<Asset::AssetStyle>(obj["style"].toDouble());
    }

    if(obj["room"].isDouble()){
        a->roomType = static_cast<Asset::AssetRoomType>(obj["room"].toDouble());
    }

    return a;
}

Asset::AssetRoomType Asset::getRoomType() const
{
    return roomType;
}
