#include "assetshelper.h"
#include <QCoreApplication>
#include <QDir>
#include <QFile>
#include <QSaveFile>
#include <QDebug>
#include <QImage>

AssetsHelper* AssetsHelper::instance = nullptr;

AssetsHelper* AssetsHelper::getAssetHelper()
{
    if(instance == nullptr) {
        instance = new AssetsHelper();
    }

    return instance;
}

AssetsHelper::AssetsHelper(QObject *parent) : QObject(parent)
{
    generateAssetFolderIfNotExists();
    generateMapFolderIfNotExists();
}

void AssetsHelper::generateAssetFolderIfNotExists()
{
    QString dirPath = QDir::cleanPath(QCoreApplication::applicationDirPath() + QDir::separator() + "Assets");
    QDir dir(dirPath);

    if(!dir.exists()){
        dir.mkpath(dirPath);
        qInfo() << "Making asset folder at " << dirPath;
        assetFolderLocation = dirPath;
        generateBaseAssets();
    }
}

void AssetsHelper::generateMapFolderIfNotExists()
{
    QString dirPath = QDir::cleanPath(QCoreApplication::applicationDirPath() + QDir::separator() + "Maps");
    QDir dir(dirPath);

    if(!dir.exists()){
        dir.mkpath(dirPath);
        qInfo() << "Making map folder at " << dirPath;
        mapFolderLocation = dirPath;
    }
}

void AssetsHelper::generateBaseAssets()
{
    {
        Asset woodWall(Asset::Walls, Asset::UnDamaged, Asset::GrassLands);
        woodWall.setName("basicWoodWall");

        QImage woodWallImg(ASSET_IMAGE_WIDTH,ASSET_IMAGE_HEIGHT,QImage::Format_ARGB32);

        woodWallImg.fill(QColor::fromRgb(0x85, 0x5E, 0x42));

        for(int i = 0; i < ASSET_IMAGE_WIDTH; i++){
            if((i % 12) < 1){
                for(int j = 0; j < ASSET_IMAGE_HEIGHT; j++){
                    if(j <28 || j > 100){
                        if(i % 12 != 0){
                            continue;
                        }
                    }
                    woodWallImg.setPixelColor(i, j,QColor::fromRgb(102,51,0));
                }
            }

        }

        woodWall.setImgPath(createAssetImagePath(&woodWall));
        woodWallImg.save(woodWall.getImgPath(), "PNG", 100);

        saveAsset(&woodWall);
    }


    {
        Asset grass(Asset::Terrain, Asset::UnDamaged, Asset::GrassLands);
        grass.setName("grass");

        QImage grassImg(ASSET_IMAGE_WIDTH, ASSET_IMAGE_HEIGHT, QImage::Format_ARGB32);
        grassImg.fill(QColor::fromRgb(17,240,10));

        for(int i = 0; i < ASSET_IMAGE_WIDTH; i++){
            if((i % 8) == 2){
                for(int j = 0; j < ASSET_IMAGE_HEIGHT; j++){
                    if((j % 10) < 3){
                        grassImg.setPixelColor(i, j,QColor::fromRgb(39,171,34));
                    }
                }
            }
        }

        grass.setImgPath(createAssetImagePath(&grass));
        grassImg.save(grass.getImgPath(), "PNG", 100);

        saveAsset(&grass);
    }

    {
        Asset hardwoodFloor(Asset::Floors, Asset::UnDamaged, Asset::GrassLands);
        hardwoodFloor.setName("hardwoodFloor");

        QImage hwFloorImg(ASSET_IMAGE_WIDTH, ASSET_IMAGE_HEIGHT, QImage::Format_ARGB32);
        hwFloorImg.fill(QColor::fromRgb(189,146,91));

        for(int i = 0; i < ASSET_IMAGE_WIDTH; i++){
                for(int j = 0; j < ASSET_IMAGE_HEIGHT; j++){
                    if(((i % 16) < 1) || (j % 32) < 1){
                        hwFloorImg.setPixelColor(i, j,QColor::fromRgb(0,0,0));
                    }
                }
        }

        hardwoodFloor.setImgPath(createAssetImagePath(&hardwoodFloor));
        hwFloorImg.save(hardwoodFloor.getImgPath(), "PNG", 100);

        saveAsset(&hardwoodFloor);
    }

    {
        Asset a(Asset::Doors, Asset::UnDamaged, Asset::GrassLands);
        a.setName("woodDoor");

        QImage img(ASSET_IMAGE_WIDTH, ASSET_IMAGE_HEIGHT, QImage::Format_ARGB32);
        img.fill(QColor::fromRgb(0,0,0,0));

        for(int i = 0; i < ASSET_IMAGE_WIDTH; i++){
                for(int j = 0; j < ASSET_IMAGE_HEIGHT; j++){
                    if((i < 30 || i > 98)){
                        img.setPixelColor(i, j,QColor::fromRgb(0x85, 0x5E, 0x42));
                    }
                }
        }

        a.setImgPath(createAssetImagePath(&a));
        img.save(a.getImgPath(), "PNG", 100);

        saveAsset(&a);
    }

    {
        Asset a(Asset::Doors, Asset::UnDamaged, Asset::GrassLands, Asset::Bedroom);
        a.setName("bed");

        QImage img(ASSET_IMAGE_WIDTH, ASSET_IMAGE_HEIGHT, QImage::Format_ARGB32);
        img.fill(QColor::fromRgb(0,0,0,0));

        for(int i = 0; i < ASSET_IMAGE_WIDTH; i++){
                for(int j = 0; j < ASSET_IMAGE_HEIGHT; j++){
                    if((i > 48 && i < 76) && (j > 50 && j < 100) ){
                        img.setPixelColor(i, j,QColor::fromRgb(214,48,15));
                    }else if((i > 48 && i < 76) && ( j > 20 && j <= 50 )){
                        img.setPixelColor(i, j,QColor::fromRgb(255,255,255));
                    }
                }
        }

        a.setImgPath(createAssetImagePath(&a));
        img.save(a.getImgPath(), "PNG", 100);

        saveAsset(&a);
    }
}

QString AssetsHelper::createAssetImagePath(Asset *a)
{
    return QDir::cleanPath(assetFolderLocation + QDir::separator() + a->getName() + "_img.png");
}

QString AssetsHelper::createAssetFilePath(Asset *a)
{
    return QDir::cleanPath(assetFolderLocation + QDir::separator() + a->getName() + "_asset.json");
}

#ifdef Q_OS_WIN

#include <Windows.h>
MEMORYSTATUSEX memory_status;

bool updateMemInfo(){
    ZeroMemory(&memory_status, sizeof(MEMORYSTATUSEX));
    memory_status.dwLength = sizeof(MEMORYSTATUSEX);
    return GlobalMemoryStatusEx(&memory_status);
}

double AssetsHelper::getPercentMemUsed()
{
    if(updateMemInfo()){
        return (double)memory_status.ullAvailPhys / (double)memory_status.ullTotalPhys;
    }
    return -1;
}

uint64_t AssetsHelper::getFreeBytes()
{
    if(updateMemInfo()){
        return memory_status.ullAvailPhys;
    }else{
        return 0;
    }
}

QString AssetsHelper::getAssetFolder()
{
    return assetFolderLocation;
}

void AssetsHelper::saveAsset(Asset *a)
{
    QByteArray jsonBytes = Asset::saveToJson(a);

    QString file = createAssetFilePath(a);

    QSaveFile sFile(file);
    sFile.open(QIODevice:: WriteOnly);
    sFile.write(jsonBytes);
    sFile.commit();
}

Asset *AssetsHelper::getAsset(Asset::AssetType type, Asset::AssetState state, Asset::AssetStyle style, Asset::AssetRoomType room)
{
    if(cachedAssets.contains(type)){
        QList<Asset*> aList = cachedAssets[type];
        for(int i = 0; i < aList.length(); i++){
            if(aList[i]->getAssetState() == state && aList[i]->getAssetStyle() == style){
                return aList[i];
            }
        }
    }

    QDir aFolder(assetFolderLocation);

    QStringList assets = aFolder.entryList(QStringList() << ".json", QDir::Files);
    Asset* anyTemp = nullptr;

    for(int i = 0; i < assets.length(); i++){
        QByteArray json = QFile(assets[i]).readAll();

        Asset* a = Asset::loadFromJson(json);

        if(type != Asset::AnyType && a->getAssetType() != type){
            if(a->getAssetType() == Asset::AnyType){
                anyTemp = a;
                continue;
            }
            delete a;
            continue;
        }

        if(state != Asset::AnyState && a->getAssetState() != state){
            if(a->getAssetState() == Asset::AnyState){
                anyTemp = a;
                continue;
            }
            delete a;
            continue;
        }

        if(style != Asset::AnyStyle && a->getAssetStyle() != style){
            if(a->getAssetStyle() == Asset::AnyStyle){
                anyTemp = a;
                continue;
            }
            delete a;
            continue;
        }

        if(room != Asset::AnyRoom && a->getRoomType() != room){
            if(a->getRoomType() == Asset::AnyRoom){
                anyTemp = a;
                continue;
            }
            delete a;
            continue;
        }

        if(getPercentMemUsed() < 80.0){
            cachedAssets[type].append(a);
        }

        if(anyTemp != nullptr){
            delete anyTemp;
        }

        return a;
    }

    if(anyTemp != nullptr){
        if(getPercentMemUsed() < 80.0){
            cachedAssets[type].append(anyTemp);
        }
        return anyTemp;
    }

    return nullptr;
}

#elif Q_OS_LINUX

double AssetsHelper::getPercentMemUsed()
{
    return 50.0;
}

uint64_t AssetsHelper::getFreeBytes()
{
    return 500;
}

#endif
