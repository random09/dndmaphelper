#ifndef ASSET_H
#define ASSET_H

#include <QObject>
#include <QImage>

class Asset : public QObject
{
    Q_OBJECT
public:

    enum AssetType {
        AnyType= -2,
        InvalidType = -1,
        Walls = 0,
        Floors,
        Terrain,
        Doors,
        Furniture
    };

    enum AssetState {
        AnyState= -2,
        InvalidState = -1,
        UnDamaged = 0,
        Damaged,
        Burning,
        Destroyed
    };

    enum AssetStyle {
        AnyStyle= -2,
        InvalidStyle = -1,
        GrassLands = 0,
        Desert
    };

    enum AssetRoomType {
        AnyRoom = -1,
        InvalidRoom = -1,
        Bedroom = 0,
        Outside
    };

    explicit Asset(QObject *parent = nullptr);
    explicit Asset(AssetType type, AssetState state, AssetStyle style, AssetRoomType roomType = AnyRoom, QObject *parent = nullptr);

    const QString &getName() const;
    void setName(const QString &newName);

    const QString &getImgPath() const;
    void setImgPath(const QString &newImgPath);

    AssetType getAssetType() const;

    AssetState getAssetState() const;

    AssetStyle getAssetStyle() const;

    QImage* getAssetImage();

    void destroyImage();

    static QByteArray saveToJson(Asset* a);
    static Asset* loadFromJson(QByteArray b);

    AssetRoomType getRoomType() const;

private:
    QString name, imgPath;

    AssetType assetType;
    AssetState assetState;
    AssetStyle assetStyle;
    AssetRoomType roomType;

    QImage* assetImage;

};

#endif // ASSET_H
