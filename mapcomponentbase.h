#ifndef MAPCOMPONENTBASE_H
#define MAPCOMPONENTBASE_H

#include <QObject>
#include <QImage>
#include <QPainter>
#include "asset.h"

class MapComponentBase : public QObject
{
    Q_OBJECT
public:
    explicit MapComponentBase(int widthTiles, int heightTiles, QObject *parent = nullptr);

    void drawTile(Asset* toDraw, int xPos, int yPos);
    QImage getComponetImage() const;
    const QPolygon &getShape() const;

    int getTileCountWidth() const;

    int getTileCountHeight() const;

    virtual void render() {};


protected:
    QImage imgBase;
    QPainter painter;
    QPolygon shape;
    int tileCountWidth, tileCountHeight;

    void finilaze();

};

#endif // MAPCOMPONENTBASE_H
