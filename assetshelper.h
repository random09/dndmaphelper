#ifndef ASSETSHELPER_H
#define ASSETSHELPER_H

#include <QObject>
#include <QMap>
#include <QList>
#include "asset.h"

class AssetsHelper : public QObject
{
    Q_OBJECT
public:

    static AssetsHelper* getAssetHelper();

    double getPercentMemUsed();
    uint64_t getFreeBytes();

    QString getAssetFolder();

    void saveAsset(Asset* a);

    Asset* getAsset(Asset::AssetType type, Asset::AssetState state = Asset::AnyState, Asset::AssetStyle style = Asset::AnyStyle, Asset::AssetRoomType room = Asset::AnyRoom);

    const static int ASSET_IMAGE_WIDTH = 128, ASSET_IMAGE_HEIGHT = 128;
private:

    QString assetFolderLocation, mapFolderLocation;
    static AssetsHelper* instance;
    explicit AssetsHelper(QObject *parent = nullptr);

    void generateAssetFolderIfNotExists();
    void generateMapFolderIfNotExists();

    void generateBaseAssets();

    QString createAssetImagePath(Asset* a);
    QString createAssetFilePath(Asset* a);

    QMap<Asset::AssetType, QList<Asset*>> cachedAssets;
};

#endif // ASSETSHELPER_H
