#ifndef MAPMODEL_H
#define MAPMODEL_H

#include "mapcomponentbase.h"
#include <QMap>

class BuildingModel;

enum TerrainEffect{
    NoTerrainEffect,
    SnowyTerrain,
    BurningTerrain
};

class MapModel : public MapComponentBase
{
public:
    explicit MapModel(int tilesX, int tilesY, QObject *parent = nullptr);

    void addBuilding(int xPos, int yPos, BuildingModel building);
    void setTerrain(Asset* ter);
    void setTerrainEffect(TerrainEffect effect, double scale);

    void setBuildRoads(bool set);

    void render() override;

private:
    TerrainEffect tEffect = NoTerrainEffect;
    int tEffectScale = 0;
    Asset* terrain;
    QMap<BuildingModel, QPoint> buildingMap;
    bool buildRoads;

};

#endif // MAPMODEL_H
