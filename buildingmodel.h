#ifndef BUILDINGMODEL_H
#define BUILDINGMODEL_H

#include "mapcomponentbase.h"
#include "roommodel.h"
#include <QMap>

class BuildingModel : public MapComponentBase
{
public:
    explicit BuildingModel(int tilesX, int tilesY, QObject *parent = nullptr);
    void addRoom(int xPos, int yPos, RoomModel room);
    void setDoorPos(int tileX, int tileY, Asset* doorObj);
    void setFloor(Asset* floor);
    void setWall(Asset* wall);

    void render() override;
private:
    QMap<RoomModel, QPoint> roomMap;
    QPoint doorPos;
    Asset* doorAsset;
    Asset* floorAsset;
    Asset* wallAsset;

};

#endif // BUILDINGMODEL_H
