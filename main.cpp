#include "mainwindow.h"

#include <QApplication>
#include "assetshelper.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow w;
    AssetsHelper* assets = AssetsHelper::getAssetHelper();
    w.show();
    return a.exec();
}
