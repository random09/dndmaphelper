#ifndef MAPGENERATOR_H
#define MAPGENERATOR_H

#include <QObject>
#include <QList>
#include <QMap>
#include <QImage>
#include "mapmodel.h"
#include "roommodel.h"
#include "buildingmodel.h"
#include "asset.h"

struct buildingTemplate;
struct roomTemplate;
struct mapTemplate;

class mapGenerator : public QObject
{
    Q_OBJECT
public:
    explicit mapGenerator(QObject *parent = nullptr);

    MapModel generateMap(mapTemplate* m); //TODO make map object contianing mapImage path, palyer positions, enemy positions, position grid and pathfinding/distance measuring  Return that object here

private:
    RoomModel generateRoom(roomTemplate* room);
    BuildingModel generateBuilding(buildingTemplate* building);

};

enum buildingType {Inn, Shop, Tavern, Home};



struct mapTemplate{
    bool drawBattleSquares;
    int minBuilding;
    int maxBuildings;

    int minMapWidth, maxMapWidth, minMapLength, maxMapLength;

    QMap<buildingType, int> minBuildingsOfType;
    QMap<buildingType, int> maxBuildingsOfType;

    QString mapName;

    Asset::AssetStyle mapStyle;

    QList<buildingTemplate> buildings;
};

struct buildingTemplate{

    mapTemplate* map;

    QString buildingName; //draw building name on map if buildings > 1

    buildingType type;
    int minRoomCount;
    int maxRoomCount;

    QList<roomTemplate> rooms;
};

struct roomTemplate{
    buildingTemplate* building;
};

#endif // MAPGENERATOR_H
