#include "buildingmodel.h"
#include "assetshelper.h"

BuildingModel::BuildingModel(int tilesX, int tilesY, QObject *parent) : MapComponentBase(tilesX, tilesY, parent)
{

}

void BuildingModel::addRoom(int xPos, int yPos, RoomModel room)
{
    room.render();
    roomMap[room] = QPoint(xPos, yPos);
}

void BuildingModel::setDoorPos(int tileX, int tileY, Asset *doorObj)
{
    doorPos = QPoint(tileX, tileY);

    doorAsset = doorObj;
}

void BuildingModel::setFloor(Asset *floor)
{
    floorAsset = floor;
}

void BuildingModel::setWall(Asset *wall)
{
    wallAsset = wall;
}

void BuildingModel::render()
{

    for(int i = 0; i < tileCountWidth; i++){
        for(int j = 0; j < tileCountHeight; j++){
            drawTile(floorAsset, i,j);
        }
    }

    for(auto iter = roomMap.begin(); iter != roomMap.end(); iter++){
        QPoint imgPos(iter.value().x() * AssetsHelper::ASSET_IMAGE_WIDTH, iter.value().y() * AssetsHelper::ASSET_IMAGE_HEIGHT);

        painter.drawImage(imgPos, iter.key().getComponetImage());
    }

    for(int i = 0; i< tileCountWidth; i++){
        drawTile(wallAsset, 0,i);
        drawTile(wallAsset, tileCountHeight-1,i);
    }

    for(int i = 0; i< tileCountHeight; i++){
        drawTile(wallAsset, i,0);
        drawTile(wallAsset, i,tileCountWidth - 1);
    }

    drawTile(floorAsset, doorPos.x(), doorPos.y());
    drawTile(doorAsset, doorPos.x(), doorPos.y());
}
