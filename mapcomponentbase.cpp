#include "mapcomponentbase.h"
#include "assetshelper.h"
#include <QPoint>

MapComponentBase::MapComponentBase(int widthTiles, int heightTiles, QObject *parent) : QObject(parent),
    imgBase(widthTiles * AssetsHelper::ASSET_IMAGE_WIDTH, heightTiles * AssetsHelper::ASSET_IMAGE_HEIGHT, QImage::Format_ARGB32),
    painter(&imgBase),
    tileCountWidth(widthTiles),
    tileCountHeight(heightTiles)
{

}

void MapComponentBase::drawTile(Asset *toDraw, int xPos, int yPos)
{
    QPoint dest(xPos * AssetsHelper::ASSET_IMAGE_WIDTH,yPos * AssetsHelper::ASSET_IMAGE_HEIGHT);

    painter.drawImage(dest, *toDraw->getAssetImage());
}

QImage MapComponentBase::getComponetImage() const
{
    return imgBase;
}

const QPolygon &MapComponentBase::getShape() const
{
    return shape;
}

int MapComponentBase::getTileCountWidth() const
{
    return tileCountWidth;
}

int MapComponentBase::getTileCountHeight() const
{
    return tileCountHeight;
}

void MapComponentBase::finilaze()
{
    painter.end();
}
